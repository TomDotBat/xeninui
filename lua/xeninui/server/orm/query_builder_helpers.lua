local Helpers
do
  local _class_0
  local _base_0 = {
    __name = "Helpers",
    __type = function(self)
      return "XeninUI.ORM.Helpers"end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self) end,
    __base = _base_0,
    ASC = "ASC",
    DESC = "DESC",
    UPSERT = true,
    NULL = {
    raw = "NULL"
    },
    raw = function(str)
      return {
      raw = str
      }
    end,
    fixArgument = function(arg)
      if (!istable(arg)) then
        return Helpers.construct(arg)
      end

      return arg
    end,
    construct = function(arg)
      return {
        mysql = arg,
        sqlite = arg
      }
    end,
    duplicate = function(arg)
      return {
        left = arg,
        right = arg
      }
    end,
    days = function(amt)
      local days = (60 * 60 * 24) * amt

      return {
        mysql = {
          left = days,
          right = "INTERVAL " .. tostring(amt) .. " DAYS"
        },
        sqlite = Helpers.duplicate(days)
      }
    end,
    timeSubtract = function(current, subtract)
      current = Helpers.fixArgument(current)
      subtract = Helpers.fixArgument(subtract)

      return {
        mysql = tostring(current.mysql.left) .. " - " .. tostring(subtract.mysql.right),
        sqlite = tostring(current.sqlite.left) .. " - " .. tostring(subtract.sqlite.right)
      }
    end,
    nowDateTime = function()
      return {
        raw = true,
        value = "now()"
      }
    end,
    now = function()
      return {
        mysql = Helpers.duplicate("UNIX_TIMESTAMP(now())"),
        sqlite = Helpers.duplicate("strftime('%s', 'now')")
      }
    end,
    cast = function(field, alias, length, typeMySQL, typeSQLite)
      if typeSQLite == nil then typeSQLite = typeMySQL
      end
      local lengthStr = ""
      if length then
        lengthStr = "(" .. tostring(length) .. ")"
      end

      local str = "CAST(" .. tostring(field) .. " AS " .. tostring(typeMySQL) .. tostring(lengthStr) .. ") AS " .. tostring(alias)

      return Helpers.construct(str)
    end,
    castChar = function(field, alias, length)
      return Helpers.cast(field, alias, length, "CHAR", "CHAR")
    end,
    count = function(fields, alias)
      local str = "COUNT(" .. tostring(fields) .. ") AS " .. tostring(alias)

      return Helpers.construct(str)
    end,
    between = function(low, high)
      return Helpers.construct("BETWEEN " .. tostring(low) .. " AND " .. tostring(high))
    end,
    sid64 = function(ply)
      if (!IsValid(ply)) then return end

      return Helpers.construct(ply:SteamID64())
    end,
    upsertDifference = function(tbl)
      return {
        raw = true,
        upsertDifference = true,
        data = tbl
      }
    end
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  Helpers = _class_0
end

XeninUI.ORM.Helpers = Helpers
