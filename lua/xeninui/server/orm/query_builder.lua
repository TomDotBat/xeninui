local Helpers = XeninUI.ORM.Helpers

local Builder
do
  local _class_0
  local _parent_0 = Helpers
  local _base_0 = {
    __name = "Builder",
    __base = Helpers.__base,
    construct = function(self, tbl)
      if (!istable(tbl)) then return tbl end
      if tbl.raw then return tbl end

      if self:isMySQL() then
        return Builder.raw(tbl.mysql)
      else
        return Builder.raw(tbl.sqlite)
      end
    end,
    getTableName = function(self)
      if istable(self.aliasTblName) then
        return self.aliasTblName[1]
      elseif isstring(self.aliasTblName) then
        return self.aliasTblName
      end

      return self.tblName
    end,
    unpackString = function(self, str)
      if istable(str) then return str end

      local split = string.Explode(" ", str)
      if (#split <= 1) then return str end

      return split
    end,
    getEnclosedTableName = function(self)
      return self:encloseValue(self:unpackString(self.aliasTblName or self.tblName))
    end,
    encloseValue = function(self, str)
      if (type(str) == "string") then
        return "`" .. tostring(str) .. "`"
      elseif (type(str) == "table") then
        local output = ""
        for i, v in ipairs(str) do
          local upperStr = v:upper()
          local isKeyword = self.keywords[upperStr]

          if isKeyword then output = output .. " " .. tostring(v) .. " "
          else
            output = output .. "`" .. tostring(v) .. "`"
          end
        end

        return output
      elseif (type(str) == "boolean") then
        return str and 1 or 0
      end

      return str
    end,
    escape = function(self, str)
      if isbool(str) then
        str = str and 1 or 0
      end

      return self.conn.SQLStr(str)
    end,
    select = function(self, ...)
      self.type = self.operations.SELECT
      self.columns = {}

      for i, v in ipairs({
      ... }) do
        if istable(v) then
          if v.raw then
            table.insert(self.columns, v.raw)
          elseif v.mysql then
            local str = self:isMySQL() and "mysql" or "sqlite"

            table.insert(self.columns, v[str])
          end
        else
          local split = string.Explode(".", v)
          local enclosed = XeninUI:Map(split, function(x)
            return self:encloseValue(self:unpackString(x))
          end)

          table.insert(self.columns, string.Implode(".", enclosed))
        end
      end

      return self
    end,
    indent = function(self, num)
      if num == nil then num = 1
      end
      local output = ""
      for i = 1, num do
        output = output .. "  "
      end

      return output
    end,
    getColumns = function(self, indentAmt)
      if indentAmt == nil then indentAmt = 1
      end
      if (!self.columns or #self.columns == 0) then return "*"end

      local output = "\n" .. self:indent(indentAmt)
      output = output .. string.Implode(",\n" .. self:indent(indentAmt), self.columns)

      return output
    end,
    getJoins = function(self)
      if (!self.joins) then return ""end

      local output = "\n"
      local size = #self.joins
      for i, v in ipairs(self.joins) do
        local leftSplit = string.Explode(".", v.leftCol)
        local leftEnclosed = XeninUI:Map(leftSplit, function(x)
          return self:encloseValue(self:unpackString(x))
        end)
        local rightSplit = string.Explode(".", v.rightCol)
        local rightEnclosed = XeninUI:Map(rightSplit, function(x)
          return self:encloseValue(self:unpackString(x))
        end)

        output = output .. (tostring(v.joinType) .. " " .. self:encloseValue(self:unpackString(v.tbl)) .. " ON " .. string.Implode(".", leftEnclosed) .. " " .. tostring(v.operator) .. " " .. string.Implode(".", rightEnclosed) .. (i == size and "" or "\n"))
      end

      return output
    end,
    orderBy = function(self, str, order)
      self.orders = self.orders or {}

      table.insert(self.orders, {
        str = str,
        order = order
      })

      return self
    end,
    getOrders = function(self)
      if (!self.orders) then return ""end

      local output = "ORDER BY"
      local size = #self.orders
      for i, v in ipairs(self.orders) do
        output = output .. ("\n" .. self:indent(1) .. self:encloseValue(v.str) .. (v.order and " " .. tostring(v.order) or "") .. (i == size and "" or ","))
      end

      return output
    end,
    join = function(self, tbl, leftCol, operator, rightCol, joinType)
      if joinType == nil then joinType = "INNER JOIN"
      end
      self.joins = self.joins or {}

      table.insert(self.joins, {
        joinType = joinType,
        tbl = tbl,
        leftCol = leftCol,
        operator = operator,
        rightCol = rightCol
      })

      return self
    end,
    leftJoin = function(self, tbl, leftCol, operator, rightCol)
      return self:join(tbl, leftCol, operator, rightCol, "LEFT JOIN")
    end,
    rightJoin = function(self, tbl, leftCol, operator, rightCol)
      return self:join(tbl, leftCol, operator, rightCol, "RIGHT JOIN")
    end,
    fullJoin = function(self, tbl, leftCol, operator, rightCol)
      return self:join(tbl, leftCol, operator, rightCol, "JOIN")
    end,
    getWheres = function(self)
      if (!self.wheres) then return ""end

      local output = "\nWHERE\n"
      local size = #self.wheres
      for i, v in ipairs(self.wheres) do
        local rightCol = v.rightCol
        if istable(rightCol) then
          if (!rightCol.raw) then
            rightCol = self:construct(rightCol)
          end

          rightCol = rightCol.raw
        else
          rightCol = self:escape(rightCol)
        end

        local leftCol = v.leftCol
        if istable(leftCol) then
          if leftCol.raw then
            leftCol = leftCol.value
          else
            leftCol = self:escape(leftCol.value)
          end
        else
          local split = string.Explode(".", leftCol)
          local columns = XeninUI:Map(split, function(x)
            return self:encloseValue(self:unpackString(x))
          end)
          leftCol = string.Implode(".", columns)
        end

        output = output .. (self:indent(1) .. leftCol .. " " .. tostring(v.operator) .. " " .. rightCol .. (i == size and "" or " AND") .. "\n")
      end

      return output
    end,
    where = function(self, leftCol, operator, rightCol)
      self.wheres = self.wheres or {}

      table.insert(self.wheres, {
        leftCol = leftCol,
        operator = operator,
        rightCol = rightCol
      })

      return self
    end,
    union = function(self, obj)
      self.unions = self.unions or {}

      table.insert(self.unions, obj)

      return self
    end,
    getUnions = function(self) end,
    groupBy = function(self, name)
      self._groupBy = name

      return self
    end,
    getGroupBy = function(self)
      if (!self._groupBy) then return ""end

      return "GROUP BY " .. self:encloseValue(self._groupBy) .. "\n"
    end,
    limit = function(self, num)
      self.limitSet = num

      return self
    end,
    getLimit = function(self)
      if (!self.limitSet) then return ""end

      return "\nLIMIT " .. self.limitSet
    end,
    offset = function(self, num)
      self.offsetSet = num

      return self
    end,
    getOffset = function(self)
      if (!self.offsetSet) then return ""end

      return "\nOFFSET " .. self.offsetSet
    end,
    debugName = function(self, name)
      self.debugName = name

      return self
    end,
    run = function(self, debug)
      if debug == nil then debug = self.debugName or "No Debug Name"
      end
      local query = self:toRaw()
      if (XeninUI.Debug == "extended") then
        print(query)
      end

      return XeninUI:InvokeSQL(self.conn, query, isstring(debug) and debug or self.tblName .. ".operation:" .. self.type)
    end,
    insert = function(self, input, upsert)
      self.type = self.operations.INSERT
      self.isUpsert = upsert
      self.insertColumns = {}

      for i, v in pairs(input) do
        if (istable(v) and v.raw) then
          if (v.upsertDifference and upsert) then
            local tbl = v.data
            for i, v in pairs(tbl) do
              if v.raw then
                tbl[i] = v.raw
              else
                tbl[i] = self:escape(v)
              end
            end

            self.insertColumns[i] = tbl
          else
            self.insertColumns[i] = v.raw
          end
        else
          self.insertColumns[i] = self:escape(v)
        end
      end

      return self
    end,
    upsert = function(self, input)
      return self:insert(input, Builder.UPSERT)
    end,
    update = function(self, input)
      self.type = self.operations.UPDATE
      self.updateColumns = {}

      for i, v in pairs(input) do
        if (istable(v) and v.raw) then
          self.updateColumns[i] = v.raw
        else
          self.updateColumns[i] = self:escape(v)
        end
      end

      return self
    end,
    delete = function(self)
      self.type = self.operations.DELETE

      return self
    end,
    toRaw = function(self)
      local ops = self.operations

      if (self.type == ops.SELECT) then
        return "SELECT " .. self:getColumns() .. "\nFROM " .. self:getEnclosedTableName() .. self:getJoins() .. self:getWheres() .. self:getGroupBy() .. self:getOrders() .. self:getOffset() .. self:getLimit()
      elseif (self.type == ops.INSERT) then
        return "INSERT INTO " .. self:getEnclosedTableName() .. " (" .. self:getInsertColumns() .. ")\n" .. "VALUES (" .. self:getValues() .. ")" .. self:getUpsert()
      elseif (self.type == ops.UPDATE) then
        return "UPDATE " .. self:getEnclosedTableName() .. "\n" .. self:indent(1) .. "SET " .. self:getUpdateStatements() .. self:getWheres() .. self:getOffset() .. self:getLimit()
      elseif (self.type == ops.DELETE) then
        return "DELETE FROM " .. self:getEnclosedTableName() .. self:getWheres() .. self:getLimit()
      end
    end,
    getValues = function(self)
      if (!self.insertColumns or table.Count(self.insertColumns) == 0) then return ""end

      local output = ""
      local size = table.Count(self.insertColumns) - 1
      local noLoops = 0
      for i, v in pairs(self.insertColumns) do
        local isLast = noLoops == size
        local str = istable(v) and v.insert or v
        output = output .. (str .. (isLast and "" or ", "))
        noLoops = noLoops + 1
      end

      return output
    end,
    getInsertColumns = function(self)
      if (!self.insertColumns or table.Count(self.insertColumns) == 0) then return ""end

      local output = ""
      local size = table.Count(self.insertColumns) - 1
      local noLoops = 0
      for i, v in pairs(self.insertColumns) do
        local isLast = noLoops == size
        output = output .. (i .. (isLast and "" or ", "))
        noLoops = noLoops + 1
      end

      return output
    end,
    getUpdateStatements = function(self)
      if (!self.updateColumns or table.Count(self.updateColumns) == 0) then return ""end

      local output = ""
      local size = table.Count(self.updateColumns) - 1
      local noLoops = 0
      for i, v in pairs(self.updateColumns) do
        local isLast = noLoops == size

        output = output .. ("\n" .. self:indent(2) .. tostring(i) .. " = " .. tostring(v) .. " " .. (isLast and "" or ","))
        noLoops = noLoops + 1
      end

      return output
    end,
    isMySQL = function(self)
      return self.conn.isMySQL()
    end,
    getUpsert = function(self)
      if (!self.isUpsert) then return ""end
      if (!self:isMySQL()) then return ""end

      local output = ""
      local size = table.Count(self.insertColumns) - 1
      local noLoops = 0
      for i, v in pairs(self.insertColumns) do
        local isLast = noLoops == size
        local str = istable(v) and v.update or v

        output = output .. ("\n" .. self:indent(2) .. i .. " = " .. str .. (isLast and "" or ", "))
        noLoops = noLoops + 1
      end

      return "\nON DUPLICATE KEY\n" .. self:indent(1) .. "UPDATE" .. output
    end,
    __type = function(self)
      return "XeninUI.ORM.Builder"end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__index)
  _class_0 = setmetatable({
    __init = function(self, tblName, conn)
      if conn == nil then conn = XeninDB
      end
      self.keywords = {
      AS = 1
      }
      self.operations = {
        SELECT = 1,
        INSERT = 2,
        UPDATE = 3,
        DELETE = 4
      }
      Builder.__parent.__init(self)

      self.tblName = tblName
      local split = string.Explode(" ", tblName)
      self.aliasTblName = split
      self.conn = conn
    end,
    __base = _base_0,
    __parent = _parent_0
  }, {
    __index = function(cls, parent)
      local val = rawget(_base_0, parent)
      if val == nil then local _parent = rawget(cls, "__parent")
        if _parent then return _parent[parent]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  if _parent_0.__inherited then _parent_0.__inherited(_parent_0, _class_0)
  end
  Builder = _class_0
end

XeninUI.ORM.Builder = Builder
